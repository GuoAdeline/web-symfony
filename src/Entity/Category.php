<?php 

namespace App\Entity;

use Cocur\Slugify\Slugify;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 * @ORM\Table(name="category")
 */
 class Category
 {
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;
	
	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\Length(max="255")
	 */
	private $name;
	
	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\Auction", mappedBy="category")
	 */
	private $auctions; 
	
	public function __construct()
	{
		$this->auctions = new ArrayCollection();
	}
	
	public function getId(): ?int
	{
		return $this->id;
	}
	
	public function getName(): ?string
	{
		return $this->name;
	}
	
	public function setName(string $name): self
	{
		$this->name = $name;
		return $this;
	}
	
	/**
	 * @return Collection[Auctions]
	 */
	public function getAuctions(): Collection
	{
		return $this->auctions;
	}
	
	public function addAuction(Auction $auction): self
	{
		if (!$this->auctions->contains($auction)) {
			$this->auctions[] = $auction;
			if ($auction->getCategory() !== $this) {
				$auction->setCategory($this);
			}
		}
		return $this;
	}
	
	public function removeAuction(Auction $auction): self
	{
		if ($this->auctions->contains($auction)) {
			$this->auctions->removeElement($auction);
			if ($auction->getCategory() === $this) {
				$auction->setCategory(null);
			}
 		}
		return $this;
	}
	
	
	
 }
 
 ?>