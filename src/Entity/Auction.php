<?php

namespace App\Entity;

use Cocur\Slugify\Slugify;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AuctionRepository")
 * @ORM\Table(name="item")
 */
 class Auction 
 {
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;
	
	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\Length(max="255")
	 */
	private $title;
	
	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\Length(max="255")
	 */
	private $author;
	
	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\Length(max="255")
	 */
	private $body;
	
	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="auctions")
	 */
	private $category; 
	
	public function __construct()
	{
	}
	
	public function getId(): ?int
	{
		return $this->id;
	}
	
	public function getTitle(): ?string
	{
		return $this->title;
	}
	
	public function setTitle(string $title): self
	{
		$this->title = $title;
		return $this;
	}
	
	public function getAuthor(): ?string
	{
		return $this->author;
	}
	
	public function setAuthor(string $author): self
	{
		$this->author = $author;
		return $this;
	}
	
	public function getBody(): ?string
	{
		return $this->body;
	}
	
	public function setBody(string $body): self
	{
		$this->body = $body;
		return $this;
	}
	
	public function getCategory(): ?Category
	{
		return $this->category;
	}
	
	public function setCategory(?Category $category): self
	{
		if ($this->category !== null) {
			$old = $this->category;
			$this->category = null;
			$old->removeAuction($this);
		}
		$this->category = $category;
		$this->category->addAuction($this);
		return $this;
	}
	
 }
 
 ?>