<?php

namespace App\Controller;

use App\Entity\Auction;
use App\Entity\Category;
use App\Repository\AuctionRepository;
use App\Repository\CategoryRepository;
use App\Form\AuctionsType;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
// use Symfony\config\routes\annotations;

class AuctionsController extends AbstractController
{
	public function __construct()
	{
	}
	
	/**
	 * Display list
	 * @Route("/auctions", name="auctions")
	 */
	public function showAuctions(Request $request, AuctionRepository $auct_repository, CategoryRepository $cat_repository): Response
	{
		return $this->render('auctions/index.html.twig', [
			'auctions' => $auct_repository->findAll(),
			'categories' => $cat_repository->findAll()
		]);
	}
	
	/**
	 * Remove auction
	 * @Route("/auction/{id}/delete", name="auction_remove", methods="GET|POST")
	 */
	public function removeAuction(Request $request, Auction $auction, AuctionRepository $repository): Response
	{
		$em = $this->getDoctrine()->getManager();
		
			$em = $this->getDoctrine()->getManager();
			$em->remove($auction);
			$em->flush();
			
		return $this->redirectToRoute('auctions');
	}
	
	/**
	 * Add auction
	 * @Route("/auction/add", name="auction_add", methods="POST")
	 */
	public function addAuction(Request $request, AuctionRepository $auct_repository, CategoryRepository $cat_repository): Response
	{
		$auction = new Auction();
		$title = $request->request->get('title');
		$author = $request->request->get('author');
		$body = $request->request->get('body');
		$categoryId = $request->request->get('category');
		$category = $cat_repository->find($categoryId);
		
		$auction->setTitle($title);
		$auction->setAuthor($author);
		$auction->setBody($body);
		if ($category != null) {
			$auction->setCategory($category);
		}
		
		$em = $this->getDoctrine()->getManager();
		$em->persist($auction);
		$em->flush();
		
		return $this->redirectToRoute('auctions');
	}
	
	/**
	 * Edit auction 
	 * @Route("/auction/{id}/edit", name="auction_edit", methods="GET|POST")
	 */
	public function editAuction(Request $request, Auction $auction, AuctionRepository $auct_repository, CategoryRepository $cat_repository): Response
	{
		$form = $this->createForm(AuctionsType::class, $auction);
		$form->handleRequest($request);
		
		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->flush();
			
			return $this->redirectToRoute('auctions');
		} else {
			return $this->render('auctions/edit.html.twig', [
				'auction' => $auction,
				'form' => $form->createView()
			]);
		}
	}
}

?>