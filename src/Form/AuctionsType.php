<?php 

namespace App\Form;

use App\Entity\Auction;
use App\Entity\Category;
use App\Repository\CategoryRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class AuctionsType extends AbstractType
{
	
	
	
	// QRO - Form used for editing auction
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add(
			'title',
			TextType::class,
			array(
				'label'=>'Title',
				'attr'=>array(
					'placeholder' => 'auction title',
					'disabled' => $options['disabled'],
				)
			)
		)
		
		->add(
			'author',
			TextType::class,
			array(
				'label'=>'Author',
				'attr'=>array(
					'placeholder' => 'auction author',
					'disabled'=>$options['disabled'],
				)
			)
		)
		
		->add(
			'body',
			TextType::class,
			array(
				'label'=>'Body',
				'attr'=>array(
					'placeholder' => 'auction body',
					'disabled'=>$options['disabled'],
				)
			)
		)
		
		->add(
			'category',
			EntityType::class,
			array(
				'label'=>'Category',
				'required'=>false,
				'class' => Category::class,
				'choice_label' => 'name',
				'query_builder' => function (CategoryRepository $cr) {
					return $cr->createQueryBuilder('c')
						->orderBy('c.name', 'ASC');
					},
				'disabled'=>$options['disabled'],
				'attr'=>array(
					'placeholder'=>'Summary of the project',
				)
			)
		);
	}
	
	
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => Auction::class,
			'disabled' => false,
			'constraints' => array(new Valid()),
		]);
	}
}

?>