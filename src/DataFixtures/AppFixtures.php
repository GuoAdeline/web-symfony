<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
// use Symfony\Bundle\FrameworkBundle\Tests\Fixtures\Validation\Category;
use App\Entity\Category;
use App\Entity\Auction;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
		
		// create categories
		$categories = [
			['electronics'],
			['appartment'],
			['pets'],
			['books'],
			['smartphone']
		];
		$objectCategory = [];
		foreach ($categories as $category){
			$c = new Category();
			$c->setName($category[0]);
			
			$manager->persist($c);
			$objectCategory[]=$c;
		}
		
		// create auctions
		$auctions = [
			['Computer', 'JY Martin', 'I sell my old computer. 3 years old'],
			['Anime', 'JY Martin', 'Looking for the end of the Fairy Tail manga ( > 126)'],
			['Elenium', 'JY Martin', 'I am looking for the french version of the The Elenium series By David Eddings'],
			['Kinect', 'JM Normand', 'I sell my new kinect that I can\'t connect to my computer'],
			['Kikou', 'M Servieres', 'My dog Kikou gave me plenty of little dogs, who wants one?'],
			['Mangas', 'M Magnin', 'I am looking for the first Alabator Mangas. Anyone get it?']
		];
		$objectAuction = [];
		foreach ($auctions as $auction){
			$d = new Auction();
			$d->setTitle($auction[0]);
			$d->setAuthor($auction[1]);
			$d->setBody($auction[2]);
			
			$manager->persist($d);
			$objectAuction[]=$d;
		}

        $manager->flush();
    }
}
?>
